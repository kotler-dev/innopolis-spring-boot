package dev.kotler.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InnopolisSpringBootApplication {
    public static void main(String[] args) {
        SpringApplication.run(InnopolisSpringBootApplication.class, args);
    }
}
