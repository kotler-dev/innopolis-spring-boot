package dev.kotler.web.forms;

import lombok.Data;

@Data
public class UserForm {
    private Integer id;
    private String firstName;
    private String lastName;
    private Integer age;
}
