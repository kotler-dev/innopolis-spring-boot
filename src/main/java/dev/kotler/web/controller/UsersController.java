package dev.kotler.web.controller;

import dev.kotler.web.forms.UserForm;
import dev.kotler.web.model.User;
import dev.kotler.web.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.websocket.server.PathParam;
import java.util.List;

// Аннотация относится к Spring
// Позволяет обрабатывать запросы
@Controller
public class UsersController {
    private final UsersService usersService;

    @Autowired
    public UsersController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("/users") // урл страницы
    public String getUsersPage(Model model) {
        List<User> users = usersService.getAllUsers();
        model.addAttribute("users", users); // аттрибут
        return "users"; // название страницы
    }


    @GetMapping("/users/{user-id}") // урл страницы
    public String getUserPage(Model model, @PathVariable("user-id") Integer userId) {
        User user = usersService.getUser(userId);
        model.addAttribute("user", user); // аттрибут
//        usersService.getUser(userId);
        return "user-edit"; // название страницы
    }

    @PostMapping("/users")
/*    public String addUser(@RequestParam("firstName") String firstName,
                          @RequestParam("lastName") String lastName,
                          @RequestParam("age") Integer age)*/

    public String addUser(UserForm userForm) {

/*        User user = User.builder()
                .firstName(userForm.getFirstName())
                .lastName(userForm.getLastName())
                .age(userForm.getAge())
                .build();
//        System.out.println(firstName + " " + lastName);

        usersRepository.save(user);*/

        usersService.addUser(userForm);

//        return "redirect:/users.html";
        return "redirect:/users";
    }

    // Post localhost/users/3/delete
    @PostMapping("/users/{user-id}/delete")
    public String deleteUser(@PathVariable("user-id") Integer userId) {
//        System.out.println("# # # -> userId -> " + userId);
        try {
//            Integer userIdInt = Integer.parseInt(userId);
            System.out.println("Integer -> userId -> " + userId);
            usersService.deleteUser(userId);
        } catch (NumberFormatException nfe) {
//            return "redirect:/users.ftlh";
            System.out.println("NumberFormatException -> userId -> " + userId);
            usersService.getAllUsers();
        }
        return "redirect:/users";
    }


    @PostMapping("/users/{user-id}/update")
    public String updateUser(UserForm userForm, @PathVariable("user-id") Integer userId) {
        usersService.updateUser(userForm);
        return "redirect:/users";
    }
}