package dev.kotler.web.services;

import dev.kotler.web.forms.UserForm;
import dev.kotler.web.model.User;
import dev.kotler.web.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UsersServiceImpl implements UsersService {
    private final UsersRepository usersRepository;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    @Override
    public void addUser(UserForm userForm) {
        User user = User.builder()
                .firstName(userForm.getFirstName())
                .lastName(userForm.getLastName())
                .age(userForm.getAge())
                .build();
//        System.out.println(firstName + " " + lastName);

        usersRepository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public void deleteUser(Integer userId) {
        usersRepository.deleteById(userId);
    }

    @Override
    public User getUser(Integer userId) {
//        return usersRepository.getById(getUserForm);
        return usersRepository.getById(userId);
//        System.out.println("--> getUser <----------------------");
    }

    @Override
    public void updateUser(UserForm userForm) {
//        System.out.println("updateUser => " + userForm);
        User user = User.builder()
                .id(userForm.getId())
                .firstName(userForm.getFirstName())
                .lastName(userForm.getLastName())
                .age(userForm.getAge())
                .build();
        usersRepository.save(user);
    }
}
