package dev.kotler.web.services;

import dev.kotler.web.forms.UserForm;
import dev.kotler.web.model.User;

import java.util.List;

public interface UsersService {
    void addUser(UserForm userForm);
    List<User> getAllUsers();
    void deleteUser(Integer userId);

    User getUser(Integer userId);

    void updateUser(UserForm userId);
}
