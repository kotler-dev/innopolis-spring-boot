/*
package dev.kotler.web.repositories;

import dev.kotler.web.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;
@Component // Помечаем на основе который Spring создат "бин компонент", который поместит в контейнер DI.
public class UsersRepositoryJdbcTemplateImpl implements UsersRepository {

    // SQL
    private static final String SQL_INSERT = "insert into account(first_name, last_name, age) values (?, ?, ?)";
    private static final String SQL_SELECT_ALL = "select * from account order by id";
    private static final String SQL_DELETE = "delete from account where id = 7";
    private static final String SQL_DELETE_BY_ID = "delete from account where id = ?";
    private static final String SQL_SELECT_BY_ID = "select * from account where id = ?";
    private static final String SQL_UPDATE = "update account set first_name = ?, last_name = ?, age = ? where id = ?";

    private static final RowMapper<User> userRowMapper = (rs, rowNum) -> {
        Integer id = rs.getInt("id");
        String firstName = rs.getString("first_name");
        String lastName = rs.getString("last_name");
        Integer age = rs.getInt("age");
        return new User(id, firstName, lastName, age);
    };

    private final JdbcTemplate jdbcTemplate;

    // DI
    @Autowired
    public UsersRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<User> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, userRowMapper);
    }

    @Override
    public void save(User user) {
        jdbcTemplate.update(SQL_INSERT, user.getFirstName(), user.getLastName(), user.getAge());
    }

    @Override
    public void deleteUser(Integer userId) {
        jdbcTemplate.update(SQL_DELETE_BY_ID, userId);
    }

    @Override
    public User findById(Integer userId) {
        return jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, userRowMapper, userId);
    }

    @Override
    public void updateUser(User user) {
        jdbcTemplate.update(SQL_UPDATE, user.getFirstName(), user.getLastName(), user.getAge(), user.getId());
    }
}
*/
