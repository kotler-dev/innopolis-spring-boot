package dev.kotler.web.repositories;

import dev.kotler.web.forms.UserForm;
import dev.kotler.web.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<User, Integer> {
    User getById(UserForm getUserForm);
}
