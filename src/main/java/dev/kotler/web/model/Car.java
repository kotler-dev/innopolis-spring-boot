package dev.kotler.web.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder // Построение запросов для моделей
@Entity // Связь с БД
@Table(name = "car") // Связываем User класс с Таблицей БД account
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String model;
    private String color;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User owner; // Замена ссылки бд на связь между таблицами в ооп

}
