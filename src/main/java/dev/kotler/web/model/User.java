package dev.kotler.web.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder // Построение запросов для моделей
@Entity // Связь с БД
@Table(name = "account") // Связываем User класс с Таблицей БД account
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // Генерируется базой данных автоматически
    private Integer id;

    private String firstName;
    private String lastName;
    private Integer age;

    @OneToMany(mappedBy = "owner") // Связь с полем owner класса Car
    private List<Car> cars; // У одного пользователя может быть много машин
}
